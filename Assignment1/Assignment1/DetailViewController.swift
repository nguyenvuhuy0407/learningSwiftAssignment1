//
//  DetaiViewController.swift
//  Assignment1
//
//  Created by HYUBYN on 12/1/15.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbLocation: UILabel!
    @IBOutlet weak var lbAddress: UILabel!
    var detailObject: DataObject?
    override func viewDidLoad() {
        super.viewDidLoad()

        lbName.text = detailObject?.name_
        lbLocation.text = "Coordinate: \(detailObject!.lat_, detailObject!.lng_)"
        lbAddress.text = detailObject?.address_
        lbAddress.lineBreakMode = .ByWordWrapping
        lbAddress.numberOfLines = 0
    }

    func setDetailItem(detailItem: DataObject){
            detailObject = detailItem
    }
    
}
