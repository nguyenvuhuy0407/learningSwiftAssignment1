//
//  DataObject.swift
//  Assignment1
//
//  Created by HYUBYN on 12/1/15.
//  Copyright © 2015 PinRide. All rights reserved.
//

import UIKit
import ObjectMapper

class DataObject: Mappable{
    
    var lat_: Float!
    var lng_: Float!
    var time_: Int!
    var id_: Int!
    var name_: String!
    var address_: String!
    var backgroundImageLink_: String?
    var type_: Int?
    var eventData_: String?
    
   
    required init?(_ map: Map){
        
    }
    
    func mapping(map: Map){
        lat_ <- map["lat"]
        lng_ <- map["lng"]
        time_ <- map["time"]
        id_  <- map["id"]
        name_ <- map["name"]
        address_ <- map["address"]
        backgroundImageLink_ <- map["backgroundImageLink"]
        type_ <- map["type"]
        eventData_ <- map["eventData"]
        
    }
}
