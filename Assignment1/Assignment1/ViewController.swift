//
//  ViewController.swift
//  Assignment1
//
//  Created by HYUBYN on 12/1/15.
//

import UIKit
import Alamofire

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var btnRequest: UIButton!
    @IBOutlet weak var tableResults: UITableView!
    var listResult = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    // handle table View
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listResult.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .Default, reuseIdentifier: "cell-default")
        let object = listResult.objectAtIndex(indexPath.row) as! DataObject
        cell.textLabel?.text = ("\(object.name_ + "-" + object.address_)")
        return cell
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let detailView = storyboard?.instantiateViewControllerWithIdentifier("showDetail") as! DetailViewController
        detailView.setDetailItem(listResult.objectAtIndex(indexPath.row) as! DataObject)
        navigationController?.pushViewController(detailView, animated: true)

    }
    // handle clicking Request Button
    @IBAction func sendRequest(sender: AnyObject) {
        startConnection()
    }
    
    // get data from url
    /*func startConnection(){
        let sessionConfig = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        let url = NSURL(string: "http://103.20.148.111:8080/RideSharing/GetPopularLocationAPI")
        let request = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "GET"
        
        let task = session.dataTaskWithRequest(request, completionHandler: { (data : NSData?, response : NSURLResponse?, error : NSError?) -> Void in
            if (error == nil) {
                // Success
                do {
                    if let jsonResult = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
                        //print(jsonResult)
                        if let results: NSDictionary = jsonResult["result"] as? NSDictionary{
                            if let listObject: NSArray = results["locations"] as? NSArray{
                                for object in listObject as! [NSDictionary]{
                                    let newObject = DataObject()
                                    newObject.createObjectFromDictionary(object)
                                    self.listResult.addObject(newObject)
                                    dispatch_async(dispatch_get_main_queue(), {
                                        self.tableResults.reloadData()
                                    })
                                }
                            }
                        }
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }

            }
            else {
                // Failure
                print("URL Session Task Failed: %@", error!.localizedDescription);
            }
        })
        task.resume()
    }*/
    func startConnection(){
        Alamofire.request(.GET, "http://103.20.148.111:8080/RideSharing/GetPopularLocationAPI")
            .responseJSON { response in
                if let JSON = response.result.value {
                    if let results: NSDictionary = JSON["result"] as? NSDictionary{
                        if let listObject: NSArray = results["locations"] as? NSArray{
                            for object in listObject as! [NSDictionary]{
                                let newObject = DataObject()
                                newObject.createObjectFromDictionary(object)
                                self.listResult.addObject(newObject)
                                dispatch_async(dispatch_get_main_queue(), {
                                    self.tableResults.reloadData()
                                })
                            }
                        }
                    }

                }
        }
    }
    
}




